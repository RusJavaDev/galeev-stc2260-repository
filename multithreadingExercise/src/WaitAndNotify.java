public class WaitAndNotify {
    private volatile int number = 0;

    public void produce() throws InterruptedException {
        while (true) {
            synchronized (this) {
                Thread.sleep(100);
                this.notify();
                number++;
                System.out.println(Thread.currentThread().getName() + " закопал");
                this.wait();
            }
        }
    }

    public void send() throws InterruptedException {
        while (true) {
            synchronized (this) {
//                Thread.sleep(800);
                this.notify();

                System.out.println(Thread.currentThread().getName() + " переносит...");
//                this.notify();
                this.wait();

            }
        }
    }

    public void consume() throws InterruptedException {
        while (true) {
            synchronized (this) {
//                Thread.sleep(800);
                this.notify();
                number--;
                System.out.println(Thread.currentThread().getName() + " откопал");
                this.wait();
            }
        }
    }
}
