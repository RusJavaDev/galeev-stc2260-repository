public class Main {
    public static void main(String[] args) throws InterruptedException {
        WaitAndNotify wn = new WaitAndNotify();

        Thread producer = new Thread(() -> {
            try {
                wn.produce();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        });

        Thread transporter = new Thread(() -> {
            try {
                wn.send();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        });

        Thread consumer = new Thread(() -> {
            try {
                wn.consume();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        });

        producer.setName("Producer");
        consumer.setName("Consumer");
        transporter.setName("Transporter");

        consumer.start();
        transporter.start();
        producer.start();
        producer.join();
        transporter.join();
        consumer.join();
    }
}
