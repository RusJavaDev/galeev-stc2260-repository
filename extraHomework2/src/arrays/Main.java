package arrays;

import java.util.*;

/*
Создать класс, в котором будут:

2.1 Метод, который принимает на вход целочисленный массив. Метод должен убрать все дубликаты в переданном массиве и вернуть массив, длина которого равна количеству оставшихся элементов.


Пример:
Массив до работы метода 	    - 		[1][1][3][6][7][7]
Массив после работы метода 	- 		[1][3][6][7]


2.2 Метод, который принимает на вход целочисленный массив. Метод должен "инвертировать" массив, т.е. - элемент который был первым - стал последним, а последний - первым, и т.д.


Пример:
Массив до работы метода 	    - 		[1][1][3][6][7][7]
Массив после работы метода 	- 		[7][7][6][3][1][1]
З.Ы. Реализация должна быть своя. Методы из "коробки", типа .reverse() использовать нельзя.
 */

public class Main {
    public static void main(String[] args) {
        int[] array = {1, 1, 3, 6, 7, 7};

        System.out.println(Arrays.toString(removeDuplicates(array)));
        System.out.println(Arrays.toString(reverseArray(array)));

    }

    public static int[] removeDuplicates(int[] array) {
        Set<Integer> set = new HashSet<>();
        for (int i : array) {
            set.add(i);
        }

        int[] result2 = new int[set.size()];
        int counter = 0;
        for (Integer e : set) {
            result2[counter] = e;
            counter++;
        }

        return result2;
    }

    public static int[] reverseArray(int[] array) {
        int[] result = new int[array.length];


        for (int i = array.length - 1, counter = 0; i >= 0; i--, counter++)
            result[counter] = array[i];


        return result;
    }
}
