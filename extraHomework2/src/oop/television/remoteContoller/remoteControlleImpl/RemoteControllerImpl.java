package oop.television.remoteContoller.remoteControlleImpl;

import oop.television.Channel;
import oop.television.Show;
import oop.television.Tv;
import oop.television.remoteContoller.RemoteController;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class RemoteControllerImpl implements RemoteController {
    private Tv tv;
    private static List<Channel> journal = new ArrayList<>();

    public RemoteControllerImpl(Tv tv) {
        this.tv = tv;
    }

    @Override
    public Channel switchOnChanel(int number, Tv tv) {
        Channel[] channels = tv.getCanals();
        Channel canal = channels[number - 1];
        journal.add(canal);

        return canal;
    }

    @Override
    public Channel switchNext(Tv tv) {
        int lastChannelsIdx = findLastChannelIdx();
        Channel nextChannels = tv.getCanals()[lastChannelsIdx + 1];
        journal.add(nextChannels);

        return nextChannels;
    }

    @Override
    public Channel switchPrevious(Tv tv) {
        int lastChannelIdx = findLastChannelIdx();
        Channel prevCanal = tv.getCanals()[lastChannelIdx - 1];
        journal.add(prevCanal);

        return prevCanal;
    }

    @Override
    public Channel switchPreviousFromMemory() throws Exception {

        if (journal.isEmpty())
            throw new Exception();


        Channel channel = journal.get(journal.size() - 2);
        journal.add(channel);

        return journal.get(journal.size() - 3);
    }

    private int findLastChannelIdx() {
        Channel[] channels = tv.getCanals();
        Channel lastChannel = journal.get(journal.size() - 1);
        int lastChannelIdx = 0;
        for (int i = 0; i < channels.length; i++) {
            if (lastChannel.equals(channels[i]))
                lastChannelIdx = i;
        }

        return lastChannelIdx;
    }

    public Show setOnTv() {
        Random random = new Random();
        int randomChannel = random.nextInt(tv.getCanals().length);
        Channel channel = tv.getCanals()[randomChannel];
        journal.add(channel);
        System.out.println(tv.getCanals()[randomChannel]);

        return tv.getCanals()[randomChannel].getShows()[0];
    }
}
