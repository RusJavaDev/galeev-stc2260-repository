package oop.television.remoteContoller;

import oop.television.Channel;
import oop.television.Show;
import oop.television.Tv;

public interface RemoteController {
    Channel switchOnChanel(int number, Tv tv);
    Channel switchNext(Tv tv);
    Channel switchPrevious(Tv tv);
    Channel switchPreviousFromMemory() throws Exception;
    Show setOnTv();
}
