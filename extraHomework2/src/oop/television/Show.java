package oop.television;

public class Show {
    private String name;

    public Show(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
