package oop.television;

import java.util.Arrays;
import java.util.Objects;

public class Channel {
    private String name;
    private Show[] shows;

    public Channel() {
    }

    public Channel(String name, Show[] shows) {
        this.name = name;
        this.shows = shows;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Show[] getShows() {
        return shows;
    }

    public void setShows(Show[] shows) {
        this.shows = shows;
    }

    @Override
    public String toString() {
        return name + "\n";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Channel canal = (Channel) o;
        return name.equals(canal.name) && Arrays.equals(shows, canal.shows);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(name);
        result = 31 * result + Arrays.hashCode(shows);
        return result;
    }
}
