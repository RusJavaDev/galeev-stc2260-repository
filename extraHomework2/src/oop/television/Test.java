package oop.television;

import oop.television.remoteContoller.RemoteController;
import oop.television.remoteContoller.remoteControlleImpl.RemoteControllerImpl;

import java.util.Scanner;

/*
Создать классы:

Класс Телевизор

Интерфейс Пульт (Сделать несколько реализаций этого интерфейса)
Класс Канал

Класс Передача


Класс Телевизор должен иметь поля:

Массив каналов
Пульт

Интерфейс Пульт должен иметь три метода:

Метод для переключения по цифрам (Т.е. ввел цифру 5 - включился 5 канал, цифру 8 - включился 8 и т.д.)
Метод для последовательного переключения вперед (Т.е. вы сейчас на 2 канале, после вызова этого метода, переключились на 3 и т.д.)
Метод для последовательного переключения назад (Т.е. вы сейчас на 3 канале, после вызова этого метода, переключились на 2 и т.д.)
Метод чтобы можно было переключаться между последними каналами (Т.е. если вы были на 5 канале, а до этого были на 8, то при вызове этого метода, он должен переключить нас на 8 канал, если вызвать его еще раз, то на 5 канал)

Класс Канал должен иметь поля:

Название канала
Массив передач

Класс Передача должен иметь поля:

Название передачи


Пример работы приложения:
При включении телевизора должен выводиться рандомный канал с передачей
Пример:

Первый канал
Вести

После этого, я могу либо переключать каналы последовательно, либо же по цифрам
Ввожу 5

Рен-ТВ
История оружия

Ввожу 10

ТНТ
Битва экстрасенсов

Ввел 0, идет последовательное переключение вперед
Ввел -1 идет последовательное переключение назад
 */

public class Test {
    public static void main(String[] args) throws InterruptedException {
        Tv television = new Tv();

        Channel firstCanal = new Channel();
        firstCanal.setName("Первый канал");
        firstCanal.setShows(new Show[]{new Show("Вести")});

        Channel russia1 = new Channel();
        russia1.setName("Россия 1");
        russia1.setShows(new Show[]{new Show("Новости")});

        Channel russia24 = new Channel();
        russia24.setName("Россия 24");
        russia24.setShows(new Show[]{new Show("День в истории")});

        Channel otr = new Channel();
        otr.setName("ОТР");
        otr.setShows(new Show[]{new Show("ОТРажение")});

        Channel renTv = new Channel();
        renTv.setName("Рен ТВ");
        renTv.setShows(new Show[]{new Show("История оружия")});

        Channel tvCentr = new Channel();
        tvCentr.setName("ТВ Центр");
        tvCentr.setShows(new Show[]{new Show("События")});

        Channel ntv = new Channel();
        ntv.setName("НТВ");
        ntv.setShows(new Show[]{new Show("Пес")});

        Channel tv3 = new Channel();
        tv3.setName("ТВ-3");
        tv3.setShows(new Show[]{new Show("Слепая")});

        Channel sts = new Channel();
        sts.setName("СТС");
        sts.setShows(new Show[]{new Show("Уральские пельмени")});

        Channel tnt = new Channel();
        tnt.setName("ТНТ");
        tnt.setShows(new Show[]{new Show("Битва экстрасенсов")});


        television.setCanals(new Channel[]{
                firstCanal,
                russia1,
                russia24,
                otr,
                renTv,
                tvCentr,
                ntv,
                tv3,
                sts,
                tnt});


        RemoteController remoteController = new RemoteControllerImpl(television);

        System.out.println("Включение телевизора:");
        Show show = remoteController.setOnTv();
        System.out.println(show);
        System.out.println("\n==============\n");


//        System.out.println("Первое переключение:");
//        Canal canal = remoteController.switchOnChanel(1, television);
//        showCanalAndShow(canal, canal.getShows()[0]);
//
//
//        System.out.println("Второе переключение:");
//        Canal canal2 = remoteController.switchOnChanel(2, television);
//        showCanalAndShow(canal2, canal2.getShows()[0]);
//
//        try {
//            System.out.println("Возвращаемся назад:");
//            Canal canal3 = remoteController.switchPreviousFromMemory();
//            showCanalAndShow(canal3, canal3.getShows()[0]);
//        } catch (Exception e) {
//            System.out.println("There was no chanel yet");
//        }
//
//        try {
//            System.out.println("Возвращаемся назад2:");
//            Canal canal4 = remoteController.switchPreviousFromMemory();
//            showCanalAndShow(canal4, canal4.getShows()[0]);
//        } catch (Exception e) {
//            System.out.println("There was no chanel yet");
//        }
//
//        try {
//            System.out.println("Возвращаемся назад3:");
//            Canal canal5 = remoteController.switchPreviousFromMemory();
//            showCanalAndShow(canal5, canal5.getShows()[0]);
//        } catch (Exception e) {
//            System.out.println("There was no chanel yet");
//        }
//
//        try {
//            System.out.println("Возвращаемся назад4:");
//            Canal canal6 = remoteController.switchPreviousFromMemory();
//            showCanalAndShow(canal6, canal6.getShows()[0]);
//        } catch (Exception e) {
//            System.out.println("There was no chanel yet");
//        }
//
//        System.out.println("Третье переключение:");
//        Canal canal7 = remoteController.switchOnChanel(5, television);
//        showCanalAndShow(canal7, canal7.getShows()[0]);
//
//        try {
//            System.out.println("Возвращаемся назад5:"); //Россия 1 - Новости
//            Canal canal8 = remoteController.switchPreviousFromMemory();
//            showCanalAndShow(canal8, canal8.getShows()[0]);
//        } catch (Exception e) {
//            System.out.println("There was no chanel yet");
//        }
//
//        System.out.println("Переключение вперед:");
//        Canal canal9 = remoteController.switchNext(television);
//        showCanalAndShow(canal9, canal9.getShows()[0]);
//
//        System.out.println("Переключение назад:");
//        Canal canal10 = remoteController.switchPrevious(television);
//        showCanalAndShow(canal10, canal10.getShows()[0]);

        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        System.out.println("Введи + или -");
        try {
            if ("+".equals(input)) {
                for (int i = 0; i < television.getCanals().length - 1; i++) {
                    Channel canal = remoteController.switchNext(television);
                    showCanalAndShow(canal, canal.getShows()[0]);
                    Thread.sleep(1000);
                }
            } else if ("-".equals(input)) {
                for (int i = 0; i < television.getCanals().length - 1; i++) {
                    Channel canal = remoteController.switchPrevious(television);
                    showCanalAndShow(canal, canal.getShows()[0]);
                    Thread.sleep(1000);
                }
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("End of channel list");
        }

    }

    private static void showCanalAndShow(Channel canal, Show show) {
        System.out.println(canal + "\n" + show);
        System.out.println("\n==============\n");
    }

}
