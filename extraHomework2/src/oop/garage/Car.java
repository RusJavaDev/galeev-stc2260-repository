package oop.garage;

import java.util.List;

public class Car {
    private String brand;
    private String model;
    private Driver driver;

    public Car(String brand, String model) {
        this.brand = brand;
        this.model = model;
    }

    public Car(String brand, String model, Driver driver) {
        this.brand = brand;
        this.model = model;
        this.driver = driver;
    }

    public Car() {
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    public void moveToGarage(List<Car> carsAbleToMove) {
        carsAbleToMove.add(this);
    }

    @Override
    public String toString() {
        return "Car{" +
                "brand='" + brand + '\'' +
                ", model='" + model + '\'' +
                ", driver=" + driver +
                '}';
    }
}
