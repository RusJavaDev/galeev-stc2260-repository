package oop.garage;

public class Garage {
    private String address;
    private Car[] cars;

    public Garage(String address, Car[] cars) {
        this.address = address;
        this.cars = cars;
    }

    public Garage() {
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Car[] getCars() {
        return cars;
    }

    public void setCars(Car[] cars) {
        this.cars = cars;
    }
}
