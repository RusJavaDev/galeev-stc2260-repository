package oop.garage;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/*
Лирическое: У нас есть гараж, в котором находятся машины. Мы покупаем новый гараж и перегоняем имеющиеся машины в новый гараж. В старом гараже их быть не должно.
Техническое: Будут четыре класса - Garage, Driver, Car и Main


Garage - имеет поля - адрес и массив машин.

Driver - имеет поля - имя.

Сar - имеет поля - марка, модель и водитель (Driver). Имеет метод - переехатьВГараж. Метод может выполняться, только если есть водитель у машины.

Main - где все будет происходить:)

Как это должно выглядеть:
К примеру, есть гараж (создан объект типа Garage и в нем массив на пять машин (Car)) на пять машин. Есть только ОДИН водитель (т.е. перегнать все машины нужно только одним водителем).
Создается новый гараж на такое же количество мест, что и старый гараж. И машины перегоняются из одного гаража в другой. Т.е. под конец программы машины находятся только в новом гараже, в старом все пусто.
 */

public class Main {
    private static List<Car> carsAbleToMove = new ArrayList<>();

    public static void main(String[] args) {
        Driver driver = Driver.getInstance("Перекуп");

        Car[] park = {new Car("Mercedes", "S500"),
                new Car("BMW", "X3", driver),
                new Car("Audi", "E-tron", driver),
                new Car("Toyota", "Silvia", driver),
                new Car("Nissan", "Qashqai", driver),
                new Car("Lada", "Vesta sport", driver)};


        Garage firstGarage = new Garage("Vladivostok", park);
        Garage resultGarage = new Garage("Moscow", new Car[park.length]);


        Car[] cars = firstGarage.getCars();


        for (int i = 0; i < cars.length; i++) {
            if (cars[i].getDriver() == null) {
                System.out.println("У " + cars[i] + " нет водителя, невозможно перегнать в новый гараж");
            } else {
                cars[i].moveToGarage(carsAbleToMove);
                cars[i] = null;
            }
        }

        System.out.println("Машины, оставшиеся в первом гараже: " + Arrays.toString(cars));


        for (int i = 0; i < carsAbleToMove.size(); i++) {
            resultGarage.getCars()[i] = carsAbleToMove.get(i);
        }

        System.out.println("Машины, приехавшие в новый гараж: " + Arrays.toString(resultGarage.getCars()));

    }
}
