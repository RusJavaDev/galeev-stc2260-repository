package oop.garage;

import java.util.Objects;

public class Driver {
    private String name;
    private static Driver driver;

    private Driver(String name) {
        this.name = name;
    }

    public static Driver getInstance(String name) {
        if (driver == null) {
            driver = new Driver(name);
            return driver;
        }

        return driver;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Override
    public String toString() {
        return "Driver{" +
                "name='" + name + '\'' +
                '}';
    }
}
