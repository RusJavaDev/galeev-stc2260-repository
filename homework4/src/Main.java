
/*
Реализовать класс со статическими методами:

* Сложение всех чисел, которые передаются в метод. Метод возвращает результат сложения.

* Вычитание всех чисел, которые передаются в метод. Метод возвращает результат вычитания.
	* Доп. задание: найти наибольшее число из чисел, которые передали в метод. И производить вычитание из него.

* Умножение всех чисел, которые передаются в метод. Метод возвращает результат умножения.

* Деление всех чисел, которые передаются в метод. При каждом следующем делении должна идти проверка:
	* что делимое число должно быть больше делителя. Если условие не выполнено, то метод возвращает текущий результат деления.
	* что делитель - положительное число. Если условие не выполнено, то метод возвращает текущий результат деления.

* Метод, высчитывающий факториал переданного числа. Должна быть проверка, что переданное число положительное.
 */


public class Main {
    public static void main(String[] args) {
        System.out.println("Result of addition: " + addition(1, 2, 3, 4, 5, 6));
        System.out.println("Result of subtraction: " + subtraction(1, 2, 4));
        System.out.println("Result of multiplication: " + multiplication(1, 2, 3));
        System.out.println("Result of division: " + division(16, 4, 2));
        try {
            System.out.println("Result of factorial: " + factorial(-1));
        } catch (RuntimeException e) {
            System.out.println(e.getMessage());
        }

    }

    public static double addition(double... numbers) {
        if (checkArray(numbers))
            return numbers[0];

        int result = 0;

        for (double number : numbers)
            result += number;


        return result;
    }

    public static double subtraction(double... numbers) {
        if (checkArray(numbers))
            return numbers[0];


        double maxValue = findMaxElement(numbers);
        double result = maxValue;

        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] == maxValue)
                numbers[i] = 0;

        }

        for (double number : numbers)
            result -= number;


        return result;

    }


    public static double multiplication(double... numbers) {
        if (checkArray(numbers)) {
            return numbers[0];
        }

        double result = 1;

        for (double number : numbers)
            result *= number;


        return result;
    }

    public static double division(double... numbers) {
        if (checkArray(numbers))
            return numbers[0];

        double result = numbers[0];
        for (int i = 1; i < numbers.length; i++) {
            if (result > numbers[i] && numbers[i] >= 0)
                result /= numbers[i];
            else
                return result;
        }
        return result;
    }

    public static int factorial(int number) {
        if (number == 0)
            return 1;

        if (number > 0) {
            double[] array = new double[number];

            for (int i = 0; i < array.length; i++)
                array[i] = i + 1;

            return (int) multiplication(array);
        }

        throw new RuntimeException("Invalid input number");
    }

    private static double findMaxElement(double... numbers) {
        double result = Double.MIN_VALUE;

        for (double number : numbers) {
            if (result < number)
                result = number;

        }

        return result;
    }

    private static boolean checkArray(double... numbers) {
        return numbers.length == 1;
    }
}
