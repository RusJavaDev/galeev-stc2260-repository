package com.stc.diploma;

import com.stc.diploma.entity.UserEntity;
import com.stc.diploma.repository.RoleRepository;
import com.stc.diploma.repository.UserRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class SecurityTest {
    private final UserRepository repository;
    private final MockMvc mockMvc;
    private final RoleRepository roleRepository;
    @Autowired
    public SecurityTest(UserRepository repository, MockMvc mockMvc, RoleRepository roleRepository) {
        this.repository = repository;
        this.mockMvc = mockMvc;
        this.roleRepository = roleRepository;
    }


    @Test
    public void status302Returned() throws Exception {
        mockMvc.perform(
                        get("http://localhost:8080/api/users"))
                .andExpect(status().is3xxRedirection());
    }
}
