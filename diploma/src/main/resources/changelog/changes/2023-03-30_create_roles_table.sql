create table roles
(
    id   bigserial
        primary key,
    name varchar(255) not null
        constraint uk_ofx66keruapi6vyqpv6f2or37
            unique
);

insert into roles(name)
values ('USER');

insert into roles(name)
values ('ADMIN');