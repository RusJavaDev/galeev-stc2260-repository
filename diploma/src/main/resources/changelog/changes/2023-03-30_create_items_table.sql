create table items
(
    id      bigserial
        primary key,
    cost    integer,
    name    varchar(255),
    user_id bigint
        constraint fkft8pmhndq1kntvyfaqcybhxvx
            references users
);