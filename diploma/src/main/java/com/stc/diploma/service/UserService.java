package com.stc.diploma.service;

import com.stc.diploma.dto.ItemDto;
import com.stc.diploma.dto.RoleDto;
import com.stc.diploma.dto.UserDto;
import com.stc.diploma.entity.UserEntity;
import com.stc.diploma.exception.UserNotFoundException;
import com.stc.diploma.exception.UserNotSavedException;
import com.stc.diploma.repository.RoleRepository;
import com.stc.diploma.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;

    public UserService(UserRepository userRepository, RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
    }


    public UserEntity addUser(UserEntity user) {
        UserEntity checkUser = userRepository.findByUsername(user.getUsername());

        if (checkUser != null) {
            throw new UserNotSavedException("User already exists!");
        }
        return userRepository.save(user);
    }

    public List<UserDto> findAll() {
        return userRepository.findAll().stream()
                .map(UserDto::fromUserEntity)
                .toList();
    }

    public UserDto getUserById(long id) {
        return UserDto.fromUserEntity(userRepository.findById(id).orElseThrow(() -> new UserNotFoundException("User wasn't found")));
    }

    public UserDto getUserByUsername(String username) {
        return UserDto.fromUserEntity(userRepository.findByUsername(username));
    }

    public UserDto updateUserRole(UserDto userDto) {
        UserEntity user = userRepository.findById(userDto.getId()).orElseThrow(() -> new UserNotFoundException("User with id = " + userDto.getId() + " wasn't found"));

        user.getRoles().clear();

        for (RoleDto role : userDto.getRole()) {
            if (role.getName().equals("ADMIN")) {
                user.setRoles(Collections.singleton(roleRepository.findByName("ADMIN")));
            } else {
                user.setRoles(Collections.singleton(roleRepository.findByName("USER")));
            }
        }

        userRepository.save(user);

        return userDto;
    }

    public void deleteUser(long id) {
        if (userRepository.existsById(id)) {
            userRepository.deleteById(id);
        }

        throw new UserNotFoundException("User isn't exist");
    }

    public List<ItemDto> getItems(long id) {
        Optional<UserEntity> userFromDb = userRepository.findById(id);

        if (userFromDb.isPresent()) {
            return userFromDb.get().getItems().stream()
                    .map(ItemDto::fromItemEntity)
                    .toList();
        } else {
            throw new UserNotFoundException("User with id = " + id + " wasn't found");
        }
    }
}
