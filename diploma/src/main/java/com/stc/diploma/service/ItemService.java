package com.stc.diploma.service;

import com.stc.diploma.dto.ItemDto;
import com.stc.diploma.entity.ItemEntity;
import com.stc.diploma.entity.UserEntity;
import com.stc.diploma.exception.ItemNotFoundException;
import com.stc.diploma.exception.UserNotFoundException;
import com.stc.diploma.repository.ItemRepository;
import com.stc.diploma.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ItemService {
    private final ItemRepository itemRepository;
    private final UserRepository userRepository;

    public ItemService(ItemRepository itemRepository, UserRepository userRepository) {
        this.itemRepository = itemRepository;
        this.userRepository = userRepository;
    }

    public List<ItemDto> getAllItems() {
        return itemRepository.findAll().stream()
                .map(ItemDto::fromItemEntity)
                .toList();
    }

    public ItemDto getById(long id) {
        return ItemDto.fromItemEntity(itemRepository.findById(id).orElseThrow(() -> new ItemNotFoundException("Item wasn't found")));
    }

    public ItemDto create(ItemDto itemDto) {
        ItemEntity itemToSave = itemRepository.save(ItemDto.fromItemDto(itemDto));
        return ItemDto.fromItemEntity(itemToSave);
    }

    public ItemDto update(ItemDto itemDto) {
        ItemEntity item = itemRepository.findById(itemDto.getId()).orElseThrow(() -> new ItemNotFoundException("Item wasn't found"));

        item.setName(itemDto.getName());
        item.setCost(itemDto.getCost());

        itemRepository.save(item);

        return ItemDto.fromItemEntity(item);
    }

    public void delete(long id) {
        itemRepository.deleteById(id);
    }

    public ItemDto setToUser(long userId, long itemId) {
        UserEntity userFromDb = userRepository.findById(userId).orElseThrow(() -> new UserNotFoundException("User with id = " + userId + " wasn't found"));
        ItemEntity itemFromDb = itemRepository.findById(itemId).orElseThrow(() -> new ItemNotFoundException("Item with id = " + userId + " wasn't found"));

        userFromDb.getItems().add(itemFromDb);
        itemFromDb.setUser(userFromDb);

        userRepository.save(userFromDb);
        itemRepository.save(itemFromDb);

        return ItemDto.fromItemEntity(itemFromDb);
    }
}
