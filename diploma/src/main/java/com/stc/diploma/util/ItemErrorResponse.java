package com.stc.diploma.util;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@AllArgsConstructor
@Getter
@Setter
public class ItemErrorResponse {
    private String message;
    private Date timestamp;
}
