package com.stc.diploma.dto;

import com.stc.diploma.entity.RoleEntity;
import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RoleDto {
    private Long id;
    private String name;

    public static RoleDto fromRoleEntity(RoleEntity role) {
        return RoleDto.builder()
                .id(role.getId())
                .name(role.getName())
                .build();
    }

}
