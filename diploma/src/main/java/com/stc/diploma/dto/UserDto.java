package com.stc.diploma.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.stc.diploma.entity.RoleEntity;
import com.stc.diploma.entity.UserEntity;
import jakarta.validation.constraints.Size;
import lombok.*;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {
    private Long id;
    private String username;
    @JsonIgnore
    private String password;
    private Set<RoleDto> role;
    private List<ItemDto> items;

    public static UserDto fromUserEntity(UserEntity user) {
        return UserDto.builder()
                .id(user.getId())
                .username(user.getUsername())
                .role(user.getRoles().stream()
                        .map(RoleDto::fromRoleEntity)
                        .collect(Collectors.toSet()))
                .items(user.getItems().stream()
                        .map(ItemDto::fromItemEntity)
                        .toList())
                .build();
    }
}
