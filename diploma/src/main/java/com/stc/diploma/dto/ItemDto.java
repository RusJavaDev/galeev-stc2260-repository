package com.stc.diploma.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.stc.diploma.entity.ItemEntity;
import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ItemDto {
    private Long id;
    private String name;
    private int cost;
    @JsonIgnore
    private UserDto user;
    private long userId;

    public static ItemDto fromItemEntity(ItemEntity item) {
        return ItemDto.builder()
                .id(item.getId())
                .name(item.getName())
                .cost(item.getCost())
                .userId(item.getUser() == null ? 0 : item.getUser().getId())
                .build();
    }

    public static ItemEntity fromItemDto(ItemDto itemDto) {
        return ItemEntity.builder()
                .id(itemDto.getId())
                .name(itemDto.getName())
                .cost(itemDto.getCost()).build();
    }
}
