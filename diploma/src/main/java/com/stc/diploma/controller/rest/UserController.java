package com.stc.diploma.controller.rest;

import com.stc.diploma.dto.ItemDto;
import com.stc.diploma.dto.UserDto;
import com.stc.diploma.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/users")
public class UserController {
    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public List<UserDto> getAllUsers() {
        return userService.findAll();
    }

    @GetMapping("/{id}")
    public UserDto getById(@PathVariable("id") long id) {
        return userService.getUserById(id);
    }

    @PutMapping()
    public UserDto updateUserRole(@RequestBody UserDto userDto) {
        return userService.updateUserRole(userDto);
    }

    @GetMapping("/{id}/items")
    public List<ItemDto> getItems(@PathVariable("id") long id) {
        return userService.getItems(id);
    }

}



