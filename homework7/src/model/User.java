package model;

import java.util.Objects;

public class User {
    private Integer id;
    private String name;
    private String surname;
    private int age;
    private boolean isWork;

    public User(String name, String surname, int age, boolean work) {
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.isWork = work;
    }

    public User(Integer id, String name, String surname, int age, boolean work) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.isWork = work;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean isWork() {
        return isWork;
    }

    public void setWork(boolean work) {
        this.isWork = work;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(id, user.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", age=" + age +
                ", isWork=" + isWork +
                '}';
    }
}
