import model.User;
import repository.UsersRepository;
import service.UsersRepositoryImpl;

public class Main {
    public static void main(String[] args) {
        Thread.setDefaultUncaughtExceptionHandler((t, e) -> System.err.println(e.getMessage()));

        UsersRepository repository = new UsersRepositoryImpl();
        repository.create(new User("Ivan", "Petrov", 35, true));
        System.out.println(repository.findById(3));
        repository.update(new User(3, "Petr", "Ivanov", 53, false));
        System.out.println(repository.findById(3));
        repository.delete(3);
        System.out.println(repository.findById(3));
    }
}
