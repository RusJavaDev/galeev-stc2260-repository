package service;

import exceptions.UserIsNotValidException;
import exceptions.UserNotFoundException;
import model.User;
import repository.UsersRepository;

import java.io.*;
import java.util.*;

public class UsersRepositoryImpl implements UsersRepository {
    private final String path = "users.txt";
    private Map<Integer, User> users = new HashMap<>();

    @Override
    public User findById(int id) {
        initUsers();
        Optional<User> result = Optional.ofNullable(users.get(id));

        return result.orElseThrow(() -> new UserNotFoundException("User with id = " + id + " wasn't found!"));
    }


    @Override
    public void create(User user) {
        initUsers();

        if (user.getId() != null)
            throw new UserIsNotValidException("Don't write id!");


        User addUser = new User((users.size() + 1), user.getName(), user.getSurname(), user.getAge(), user.isWork());

        try (BufferedWriter bw = new BufferedWriter(new FileWriter(path, true))) {
            bw.write(addUser.getId() + "|" + addUser.getName() + "|" + addUser.getSurname() + "|" + addUser.getAge() + "|" + addUser.isWork() + "\n");
        } catch (IOException e) {
            System.err.println("New user wasn't create!");
        }
    }

    @Override
    public void update(User user) {
        initUsers();

        if (user.getId() == null)
            throw new UserIsNotValidException("You should write id!");


        Optional<User> getUserFromMap = Optional.ofNullable(users.get(user.getId()));

        if (getUserFromMap.isPresent()) {
            User userToUpdate = getUserFromMap.get();
            userToUpdate.setName(user.getName());
            userToUpdate.setSurname(user.getSurname());
            userToUpdate.setAge(user.getAge());
            userToUpdate.setWork(user.isWork());
        } else {
            throw new UserNotFoundException("User with id = " + user.getId() + " wasn't found!");
        }

        updateData();
    }

    @Override
    public void delete(int id) {
        initUsers();

        if (users.remove(id) != null)
            updateData();
         else
            throw new UserNotFoundException("User with id = " + id + " wasn't found!");
    }

    private void updateData() {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<Integer, User> entry : users.entrySet()) {
            sb
                    .append(entry.getKey())
                    .append("|")
                    .append(entry.getValue().getName())
                    .append("|")
                    .append(entry.getValue().getSurname())
                    .append("|")
                    .append(entry.getValue().getAge())
                    .append("|")
                    .append(entry.getValue().isWork())
                    .append("\n");
        }
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(path))) {
            bw.write(sb.toString());
        } catch (IOException e) {
            System.err.println("Couldn't write file!");
        }
    }

    private void initUsers() {
        try (BufferedReader br = new BufferedReader(new FileReader(path))) {
            while (br.ready()) {
                String[] lines = br.readLine().split("\\|");
                users.put(Integer.parseInt(lines[0]), new User(lines[1], lines[2], Integer.parseInt(lines[3]), Boolean.parseBoolean(lines[4])));
            }
        } catch (IOException e) {
            System.err.println("Something went wrong!");
        }
    }
}
