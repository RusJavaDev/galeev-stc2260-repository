import com.sun.source.tree.IfTree;

import java.util.Objects;

public class Human {
    private String name;
    private String lastName;
    private String patronymic;
    private String city;
    private String street;
    private String house;
    private String flat;
    private String numberPassport;

    public Human(String name,
                 String lastName,
                 String patronymic,
                 String city,
                 String street,
                 String house,
                 String flat,
                 String numberPassport) {
        this.name = name;
        this.lastName = lastName;
        this.patronymic = patronymic;
        this.city = city;
        this.street = street;
        this.house = house;
        this.flat = flat;
        this.numberPassport = numberPassport.replaceAll(" ", "");
    }

    @Override
    public String toString() {
        try {
            int[] passport = processPassportInfo();
            return lastName + " " + name + " " + patronymic + "\n"
                    + "Паспорт: \n" +
                    "Серия: " + passport[0] + " " + passport[1] + " Номер: " + passport[2] + "\n" +
                    "Город " + city + ", ул. " + street + ", дом " + house + ", квартира " + Integer.parseInt(flat);
        } catch (Exception e) {
            return "Некорректные данные. Проверьте номер квартиры, серию и номер паспорта";
        }
    }

    private int[] processPassportInfo() throws Exception {
        int[] result = new int[3];

        if (numberPassport.length() != 10)
            throw new Exception();

        int serial1 = Integer.parseInt(numberPassport.substring(0, 2));
        int serial2 = Integer.parseInt(numberPassport.substring(3, 5));
        int number = Integer.parseInt(numberPassport.substring(4));

        result[0] = serial1;
        result[1] = serial2;
        result[2] = number;

        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return numberPassport.equals(human.numberPassport);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, lastName, patronymic, city, street, house, flat, numberPassport);
    }
}
