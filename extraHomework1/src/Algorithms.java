import java.util.Arrays;
import java.util.Random;

public class Algorithms {
    public static void main(String[] args) {
        short[] array = generateSequence(); //использовал массив шортов, т.к. при создании массива интов ловил Out of memory: heap space.
        System.out.printf("Minimal element: %d", findMin(array));
        quantityOfEvenAndOddElements(array);
    }

    public static short[] generateSequence() {
        Random random = new Random();
        short[] array = new short[(short) (random.nextInt(Short.MAX_VALUE))];

        for (int i = 0; i < array.length - 2; i++)
            array[i] = (short) random.nextInt();


        array[array.length - 1] = -1;

        return array;
    }

    public static short findMin(short[] array) {
        short min = Short.MAX_VALUE;
        for (short value : array) {
            if (min > value)
                min = value;
        }

        return min;
    }

    public static void quantityOfEvenAndOddElements(short[] array) {
        int evenCounter = 0;
        int oddCounter = 0;

        for (short value : array) {
            if (value % 2 == 0)
                evenCounter++;
            else
                oddCounter++;
        }

        System.out.printf("\nThere is %d even elements, %d odd elements", evenCounter, oddCounter);
    }
}
