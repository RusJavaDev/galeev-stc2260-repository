import entity.Student;
import entity.Teacher;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {
    @Test
    public void equalsAndHashcodeTest() {
        Configuration configuration = new Configuration().addAnnotatedClass(Teacher.class)
                .addAnnotatedClass(Student.class);

        try (SessionFactory sessionFactory = configuration.buildSessionFactory();
             Session session = sessionFactory.getCurrentSession()) {
            session.beginTransaction();

            Teacher teacher = new Teacher();
            teacher.setFirstName("Светлана");
            teacher.setLastName("Трубникова");

            Student student1 = new Student();
            student1.setFirstName("Рустем");
            student1.setLastName("Галеев");

            Student student2 = new Student();
            student2.setFirstName("Рустем");
            student2.setLastName("Галеев");

            teacher.addStudent(student1);
            teacher.addStudent(student2);


            session.save(teacher);

            Student student3 = session.get(Student.class, 1L);
            Student student4 = session.get(Student.class, 2L);
            assertNotEquals(student3.getId(), student4.getId());
            assertTrue(teacher.getStudents().contains(student1));
            assertTrue(teacher.getStudents().contains(student2));


            session.getTransaction().commit();
        }

    }


}