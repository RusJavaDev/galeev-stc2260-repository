import entity.Student;
import entity.Teacher;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


public class Main {
    public static void main(String[] args) {
        Configuration configuration = new Configuration().addAnnotatedClass(Teacher.class)
                .addAnnotatedClass(Student.class);

        try (SessionFactory sessionFactory = configuration.buildSessionFactory();
             Session session = sessionFactory.getCurrentSession()) {
            session.beginTransaction();

            initDb(session);
            showTeacherListForStudent(1, session);
            System.out.println();
            showStudentListForTeacher(3, session);

            session.getTransaction().commit();
        }
    }

    public static void initDb(Session session) {
        Student student1 = new Student();
        student1.setFirstName("Галеев");
        student1.setLastName("Рустем");

        Student student2 = new Student();
        student2.setFirstName("Илья");
        student2.setLastName("Молчанов");

        Student student3 = new Student();
        student3.setFirstName("Данил");
        student3.setLastName("Савкин");

        Student student4 = new Student();
        student4.setFirstName("Игорь");
        student4.setLastName("Алфимов");

        Student student5 = new Student();
        student5.setFirstName("Олеся");
        student5.setLastName("Холина");

        Teacher teacher1 = new Teacher();
        teacher1.setFirstName("Светлана");
        teacher1.setLastName("Трубникова");

        Teacher teacher2 = new Teacher();
        teacher2.setFirstName("Людмила");
        teacher2.setLastName("Русанова");

        Teacher teacher3 = new Teacher();
        teacher3.setFirstName("Олег");
        teacher3.setLastName("Игонин");

        student1.addTeacher(teacher1);
        student1.addTeacher(teacher3);
        student2.addTeacher(teacher3);
        student3.addTeacher(teacher3);
        student4.addTeacher(teacher2);
        student5.addTeacher(teacher2);

        session.save(teacher1);
        session.save(teacher2);
        session.save(teacher3);
    }

    public static void showStudentListForTeacher(long teacherId, Session session) {
        Teacher teacher = session.get(Teacher.class, teacherId);

        System.out.printf("%s %s %s %s %s ", "Преподаватель", teacher.getFirstName(), teacher.getLastName(), ", студенты:", teacher.getStudents());
    }

    public static void showTeacherListForStudent(long studentId, Session session) {
        Student student = session.get(Student.class, studentId);
        System.out.printf("%s %s %s %s %s ", "Студент", student.getFirstName(), student.getLastName(), ", преподаватели:", student.getTeachers());
    }
}
