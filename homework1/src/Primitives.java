import java.util.InputMismatchException;
import java.util.Scanner;

public class Primitives {
    public static void main(String[] args) {
        primitives();
        findMaxDigit();
    }

    public static void primitives() {
        System.out.println("Тип переменной | Размер в битах |               Диапазон значений от..до                      | Значение по-умолчанию |");
        System.out.printf("%s\t\t\t\t\t%d\t\t\t\t\t\t\t    %s\t\t\t\t\t\t\t\t\t %s\n", "boolean", 1, "true или false", "false");
        System.out.printf("%s\t\t\t\t\t%d\t\t\t\t\t\t\t   %s\t\t\t\t\t\t\t\t\t\t %d\n", "byte", 8, "-128..127", 0);
        System.out.printf("%s\t\t\t\t\t%d\t\t\t\t\t\t\t   %s\t\t\t\t\t\t\t\t\t %d\n", "short", 16, "-32768..32767", 0);
        System.out.printf("%s\t\t\t\t\t\t%d\t\t\t\t\t\t%s\t\t\t\t\t\t\t %d\n", "int", 32, "-2,147,483,648..2,147,483,647", 0);
        System.out.printf("%s\t\t\t\t\t%d\t\t\t%s\t\t\t\t %d\n", "long", 64, "-9,223,372,036,854,775,808..9,223,372,036,854,775,807", 0);
        System.out.printf("%s\t\t\t\t\t%d\t\t\t\t\t\t\t\t%s\t\t\t\t\t\t\t\t\t\t %s\n", "char", 16, "0..65,535", "'\\u0000'");
        System.out.printf("%s\t\t\t\t\t%d\t\t\t\t\t\t\t%s\t\t\t\t\t\t\t\t\t %.1f\n", "float", 32, "-3.4E+38..3.4E+38", 0.0);
        System.out.printf("%s\t\t\t\t\t%d\t\t\t\t\t\t\t%s\t\t\t\t\t\t\t\t\t %.1f\n\n", "double", 64, "-1.7E+308..1.7E+308", 0.0);
    }

    public static void findMaxDigit() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введи число:");
        try {
            int number = Math.abs(scanner.nextInt());

            int maxDigit = 0;
            int temp;

            while (number != 0) {
                temp = number % 10;

                if (maxDigit < temp)
                    maxDigit = temp;

                number /= 10;
            }

            System.out.printf("Самая большая цифра - %d", maxDigit);
        } catch (InputMismatchException e) {
            System.out.println("Нужно ввести число");
        }
    }
}
