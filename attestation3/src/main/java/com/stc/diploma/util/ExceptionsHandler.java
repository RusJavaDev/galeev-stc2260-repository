package com.stc.diploma.util;

import com.stc.diploma.exception.ItemNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.Date;

@ControllerAdvice
public class ExceptionsHandler {
    @ExceptionHandler(ItemNotFoundException.class)
    public ResponseEntity<ItemErrorResponse> globalExceptionsHandler(RuntimeException e) {
        ItemErrorResponse response = new ItemErrorResponse(e.getMessage(), new Date(System.currentTimeMillis()));

        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }
}
