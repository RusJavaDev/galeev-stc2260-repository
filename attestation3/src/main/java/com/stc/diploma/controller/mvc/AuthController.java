package com.stc.diploma.controller.mvc;

import com.stc.diploma.dto.UserDto;
import com.stc.diploma.entity.RoleEntity;
import com.stc.diploma.entity.UserEntity;
import com.stc.diploma.service.RoleService;
import com.stc.diploma.service.UserService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.Collections;

@Controller
public class AuthController {
    private final UserService userService;
    private final RoleService roleService;
    private final PasswordEncoder passwordEncoder;

    public AuthController(UserService userService, RoleService roleService, PasswordEncoder passwordEncoder) {
        this.userService = userService;
        this.roleService = roleService;
        this.passwordEncoder = passwordEncoder;
    }

    @GetMapping("/")
    public String home() {
        return "home";
    }

    @GetMapping("/register")
    public String register(Model model) {
        model.addAttribute("user", new UserDto());
        return "register";
    }

    @PostMapping("/register")
    public String registerSubmit(@ModelAttribute UserDto user) {
        UserEntity userToSave = UserEntity.builder()
                .username(user.getUsername())
                .password(passwordEncoder.encode(user.getPassword()))
                .build();
        RoleEntity role = roleService.findByName("USER");

        if (role == null) {
            RoleEntity roleToSave = new RoleEntity("USER");
            roleService.saveRole(roleToSave);
        }

        userToSave.setRoles(Collections.singleton(role));
        userService.addUser(userToSave);
        return "redirect:/login";
    }

    @GetMapping("/login")
    public String showLoginPage(Model model) {
        model.addAttribute("user", new UserDto());
        return "login";
    }

    @PostMapping("/login")
    public String loginSubmit(@ModelAttribute("user") UserDto user) {
        if (userService.getUserByUsername(user.getUsername()) != null) {
            return "redirect:/";
        }
        return "redirect:/login";
    }
}
