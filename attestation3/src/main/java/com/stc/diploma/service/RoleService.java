package com.stc.diploma.service;

import com.stc.diploma.entity.RoleEntity;
import com.stc.diploma.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleService {
    private final RoleRepository roleRepository;
    @Autowired
    public RoleService(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    public RoleEntity findByName(String name) {
        return roleRepository.findByName(name);
    }

    public void saveRole(RoleEntity role) {
        roleRepository.save(role);
    }
}
