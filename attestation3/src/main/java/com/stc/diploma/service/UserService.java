package com.stc.diploma.service;

import com.stc.diploma.dto.UserDto;
import com.stc.diploma.entity.UserEntity;
import com.stc.diploma.exception.UserNotFoundException;
import com.stc.diploma.exception.UserNotSavedException;
import com.stc.diploma.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {
    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    public UserEntity addUser(UserEntity user) {
        UserEntity checkUser = userRepository.findByUsername(user.getUsername());

        if (checkUser != null) {
            throw new UserNotSavedException("User already exists!");
        }
        return userRepository.save(user);
    }

    public List<UserDto> getAllUsers() {
        return userRepository.findAll().stream()
                .map(UserDto::fromUserEntity)
                .toList();

    }

    public UserDto getUserById(long id) {
        return UserDto.fromUserEntity(userRepository.findById(id).orElseThrow(() -> new UserNotFoundException("User wasn't found")));
    }

    public UserDto getUserByUsername(String username) {
        return UserDto.fromUserEntity(userRepository.findByUsername(username));
    }

    public UserDto updateUser(UserDto userDto) {
        UserEntity user = userRepository.findById(userDto.getId()).orElseThrow(() -> new UserNotFoundException("User with id = " + userDto.getId() + " wasn't found"));
        user.setUsername(userDto.getUsername());
        user.setPassword(userDto.getPassword());

        userRepository.save(user);

        return userDto;
    }

    public void deleteUser(long id) {
        if (userRepository.existsById(id)) {
            userRepository.deleteById(id);
        }

        throw new UserNotFoundException("User isn't exist");
    }

}
