package com.stc.diploma.dto;

import com.stc.diploma.entity.ItemEntity;
import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ItemDto {
    private Long id;
    private String name;
    private int cost;

    public static ItemDto fromItemEntity(ItemEntity item) {
        return ItemDto.builder()
                .id(item.getId())
                .name(item.getName())
                .cost(item.getCost())
                .build();
    }

    public static ItemEntity fromItemDto(ItemDto itemDto) {
        return ItemEntity.builder()
                .id(itemDto.getId())
                .name(itemDto.getName())
                .cost(itemDto.getCost()).build();
    }
}
