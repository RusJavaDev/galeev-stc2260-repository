import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Figure[] figures = {new Ellipse(3,1),
                new Rectangle(2,2),
                new Square(3),
                new Circle(7)};

        Arrays.stream(figures)
                .forEach(i -> {
                    if (i instanceof Movable) {
                        System.out.print("Movable figure is found: " + i.getClass().getSimpleName() + ". Moving it... It's new coordinates: ");
                        ((Movable) i).move((int) (Math.random() * 10), (int) (Math.random() * 10));
                        System.out.print("(" + i.getX() + ", " + i.getY() + "). It's perimeter: " + i.getPerimeter() + ".\n");
                    } else {
                        System.out.print("Figure is found: " + i.getClass().getSimpleName() + ". It's perimeter: " + i.getPerimeter() + ".\n");
                    }
                });
    }
}
