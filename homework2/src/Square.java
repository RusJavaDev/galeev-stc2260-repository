public class Square extends Rectangle implements Movable {
    private double length;

    public Square() {
    }

    public Square(double length) {
        if (length > 0)
            this.length = length;
    }

    @Override
    public double getPerimeter() {
        return 4 * length;
    }

    @Override
    public void move(int x, int y) {
        super.x = x;
        super.y = y;
    }

    @Override
    public double getLength() {
        return length;
    }

    @Override
    public void setLength(double length) {
        this.length = length;
    }

    @Override
    public int getX() {
        return super.getX();
    }

    @Override
    public int getY() {
        return super.getY();
    }
}
