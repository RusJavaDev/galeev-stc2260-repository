public class Ellipse extends Figure {
    private double majorAxis, minorAxis;

    public Ellipse() {
    }

    public Ellipse(double majorAxis, double minorAxis) {
        if (majorAxis > 0 && minorAxis > 0) {
            this.majorAxis = majorAxis;
            this.minorAxis = minorAxis;
        }
    }

    @Override
    public double getPerimeter() {
        return 4 * ((Math.PI * majorAxis * minorAxis + (majorAxis - minorAxis)) / (majorAxis + minorAxis));
    }

    public double getMajorAxis() {
        return majorAxis;
    }

    public void setMajorAxis(double majorAxis) {
        this.majorAxis = majorAxis;
    }

    public double getMinorAxis() {
        return minorAxis;
    }

    public void setMinorAxis(double minorAxis) {
        this.minorAxis = minorAxis;
    }
}
