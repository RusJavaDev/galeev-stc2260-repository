public abstract class Figure {
    protected int x, y;

    public Figure() {
    }

    public abstract double getPerimeter();

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
