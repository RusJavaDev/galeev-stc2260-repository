public class Circle extends Ellipse implements Movable {
    private double radius;

    public Circle() {
    }

    public Circle(double radius) {
        if (radius > 0)
            this.radius = radius;
    }

    @Override
    public double getPerimeter() {
        return 2 * Math.PI * radius;
    }

    @Override
    public void move(int x, int y) {
        super.x = x;
        super.y = y;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public int getX() {
        return super.getX();
    }

    @Override
    public int getY() {
        return super.getY();
    }
}
