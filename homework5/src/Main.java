import java.util.Arrays;

/*
Предусмотреть функциональный интерфейс:

> interface ByCondition {
>
>     boolean isOk(int number);
>
> }

Написать класс **Sequence**, в котором должен присутствовать метод **filter**:

> public static int[] filter(int[] array, ByCondition condition) {
>
>   ...
>
> }

Данный метод возвращает массив, который содержит элементы, удовлетворяющиие логическому выражению в **condition**.

В main в качестве **condition** подставить:

- проверку на четность элемента
- проверку, является ли сумма цифр элемента четным числом.
- Доп. задача: проверка на четность всех цифр числа. (число состоит из цифр)
- Доп. задача: проверка на палиндромность числа (число читается одинаково и слева, и справа -> 11 - палиндром, 12 - нет, 3333 - палиндром, 3211 - нет, и т.д.).
 */

public class Main {
    public static void main(String[] args) {
        int[] array = {24, 21, 33, 43, 1111};
        System.out.println("Здесь только четные элементы: " + Arrays.toString(Sequence.filter(array, i -> i % 2 == 0)));
        System.out.println("Сумма цифр этих элементов - четное число: " + Arrays.toString(Sequence.filter(array, i -> {
            int sum = 0;

            for (int j = i; j > 0; j /= 10)
                sum += j % 10;

            return sum % 2 == 0;
        })));

        System.out.println("В этих элементах каждая цифра - четная: " + Arrays.toString(Sequence.filter(array, i -> {
            boolean flag = false;

            int number = i;

            while (number > 0) {
                int digit = number % 10;

                if (digit % 2 == 0)
                    flag = true;
                else {
                    flag = false;
                    break;
                }

                number /= 10;
            }

            return flag;
        })));

        System.out.println("Все элементы - палиндромы: " + Arrays.toString(Sequence.filter(array, i -> {
            int firstDigit = i % 10;
            boolean flag = false;


            int number = i / 10;

            while (number > 0) {
                if (number % 10 == firstDigit)
                    flag = true;
                else {
                    flag = false;
                    break;
                }

                number /= 10;
            }

            return flag;
        })));
    }
}
